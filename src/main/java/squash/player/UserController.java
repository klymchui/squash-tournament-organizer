package squash.player;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("user")
@RequiredArgsConstructor
@Slf4j
public class UserController {

    private final UserRepository userRepository;

    @PostMapping
    public void addUser(@Valid @RequestBody User user) {
        log.info("Adding user: {}", user);
        userRepository.insert(user);
    }

    @GetMapping("/all")
    public List<User> getUsers(){
        log.info("Get all users");
        return userRepository.findAll();
    }

    @GetMapping
    public List<User> getUser(@RequestParam String lastName, @RequestParam String firstName){
        log.info(String.format("Get users with name: %s %s", lastName, firstName));
        return userRepository.findByLastNameAndFirstName(lastName, firstName);
    }

    @GetMapping("/{id}")
    public Optional<User> getUser(@PathVariable String id){
        log.info(String.format("Get user with id: %s", id));
        return userRepository.findById(id);
    }

    @DeleteMapping("/{id}")
    public void removeUser(@PathVariable String id) {
        log.info(String.format("Remove user with id: %s", id));
        userRepository.removeById(id);
    }

    @PutMapping("/update")
    public void updateUser(@Valid @RequestBody User user){
        log.info(String.format("Update user with id: %s", user.getId()));
        userRepository.save(user);
    }
}
