package squash.player;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UserRepository extends MongoRepository<User, String> {

    void removeById(String id);

    List<User> findByLastNameAndFirstName(String lastName, String firstName);
}
