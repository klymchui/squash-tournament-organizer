package squash.player;

public enum Role {
    GUEST,
    ADMIN,
    PLAYER,
    ORGANIZER,
    REFEREE;
}
