package squash.tournament;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PreDestroy;
import java.util.List;

@RestController
@RequestMapping("tournament")
@RequiredArgsConstructor
@Slf4j
public class TournamentController {

    private final TournamentRepository tournamentRepository;

    @GetMapping
    public List<Tournament> getTournaments() {
        log.info("get tournaments");
        return tournamentRepository.findAll();
    }

    @PostMapping
    public void addTournament(@RequestBody Tournament tournament) {
        log.info("add tournament {}", tournament);
        tournamentRepository.insert(tournament);
    }

    @PreDestroy
    public void test() {
        log.warn("Bye World!!!");
    }
}
