package squash.tournament;

import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class Tournament {

    @Id
    private String id;
    private String name;
}
