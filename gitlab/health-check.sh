#!/usr/bin/env bash

url='http://ec2-18-216-61-191.us-east-2.compute.amazonaws.com:8080/actuator/health'
status=$(curl --write-out %{http_code} --silent --output /dev/null ${url})
if [[ "$status" -ne 200 ]] ; then
  echo "Site status changed to $status"
  exit 1
else
  exit 0
fi