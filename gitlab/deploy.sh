#!/bin/bash

# any future command that fails will exit the script
set -e

# Lets write the public key of our aws instance
eval $(ssh-agent -s)
echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null

# disable the host key checking.
mkdir -p ~/.ssh
touch ~/.ssh/config
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config

#echo "deploying to ${DEPLOY_SERVERS}"
ssh ec2-user@$DEPLOY_SERVERS "curl -L https://gitlab.com/klymchui/squash-tournament-organizer/-/jobs/artifacts/master/raw/build/libs/tournament-organizer-1.jar?job=build > app.jar && ./restarter.sh"